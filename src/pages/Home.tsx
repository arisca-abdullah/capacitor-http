import React, { useState } from 'react';
import {
  IonContent,
  IonHeader,
  IonList,
  IonItem,
  IonLabel,
  IonCheckbox,
  IonPage,
  IonTitle,
  IonToolbar,
  IonAlert,
  IonFab,
  IonFabButton,
  IonIcon,
  useIonViewWillEnter
} from '@ionic/react';
import { addOutline } from 'ionicons/icons';
import '@capacitor-community/http';
import { Plugins } from '@capacitor/core';
import { HttpResponse } from '@capacitor-community/http';

const Home: React.FC = () => {
  const [todos, setTodos] = useState<any[]>([]);
  const [showAlert, setShowAlert] = useState<boolean>(false);

  useIonViewWillEnter(() => {
    setTimeout(() => {
      Plugins.Http.request({
        method: 'GET',
        url: 'https://jsonplaceholder.typicode.com/todos?_limit=10',
        headers: {},
        params: {}
      }).then((resp: HttpResponse) => {
        setTodos(resp.data);
      });
    }, 3000);
  });

  const addTodo: (data: any) => Promise<HttpResponse> = (data: any) => {
    return Plugins.Http.request({
      method: 'POST',
      url: 'https://jsonplaceholder.typicode.com/todos',
      headers: {},
      data
    });
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Capacitor HTTP</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonList>
          {
            todos.map((todo: any) => (
              <IonItem key={todo.id}>
                <IonLabel>{ todo.title }</IonLabel>
                <IonCheckbox slot="start" checked={todo.completed} />
              </IonItem>
            ))
          }
        </IonList>

        <IonAlert
          isOpen={showAlert}
          onDidDismiss={() => setShowAlert(false)}
          header={'Add Todo'}
          inputs={[
            {
              type: 'text',
              name: 'title',
              placeholder: 'Your task',
              label: 'Title'
            }
          ]}
          buttons={[
            {
              text: 'Cancel',
              role: 'cancel',
            },
            {
              text: 'Add',
              handler: (value: any) => {
                if (value?.title) {
                  const data = { title: value.title, completed: false }
                  addTodo(data).then((value: HttpResponse) => {
                    setTodos([...todos, { ...data, id: value.data.id }]);
                  });
                }
              }
            }
          ]}
        />

        <IonFab vertical="bottom" horizontal="end" slot="fixed">
          <IonFabButton onClick={() => setShowAlert(true)}>
            <IonIcon icon={addOutline} />
          </IonFabButton>
        </IonFab>
      </IonContent>
    </IonPage>
  );
};

export default Home;
